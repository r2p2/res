all:
	g++ -o out -fsanitize=address -fsanitize=leak -fsanitize=vptr -fsanitize=null -fsanitize=return -std=c++20 -O0 -ggdb main.cc
	./out
