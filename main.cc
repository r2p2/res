#include "carrier.hpp"

int main()
{
    Carriers carriers;
    auto& c1 = carriers.add("E00712345");
    // c1 is immuable and can be inspected

    std::cout << c1.str() << std::endl;
    
    // option 1
    {
        auto c1_mut = c1.mutate();
        c1_mut.modify()
            .loc_curr("new loc")
            .state(Carrier::State::Transferring);
        //  .update(); for manual updates either
        // c1_mut.modify().update(); or that
    }

    std::cout << "---" << std::endl;

    // option 2
    {
        c1.mutate()
            .modify()
            .loc_curr("new loc")
            .state(Carrier::State::Transferring);
        // update is done automatically when modification is detected
    }

    std::cout << c1.str() << std::endl;

    carriers.add("s1").mutate().modify().state(Carrier::State::Stored);
    carriers.add("s2").mutate().modify().state(Carrier::State::WaitOut);
    carriers.add("s3").mutate()->state(Carrier::State::Stored);

    std::cout << "stored carriers:" << std::endl;
    for (auto const& carrier : carriers |
        std::views::filter([](Carrier const& c) { return c.is_stored(); }))
    {
        std::cout << " - " << carrier.id() << std::endl;
    }

    auto s2 = carriers.find_by_id("s2");
    if (s2 != nullptr)
    {
		auto const s2_kept = s2->keep();
		if (s2_kept.is_valid()) {
			std::cout << s2_kept->str() << std::endl;
		}
		s2->mutate().modify().remove();       

		if (s2_kept.is_valid()) {
			std::cout << s2_kept->str() << std::endl;
		}
		else {
			std::cout << "not valid anymore" << std::endl;
		}
    }


    return 0;
}
