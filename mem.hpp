#pragma once
// current state:
// std::vector<std::shared_ptr<db::Carrier>> | contains database representation scattered through memory
// type::Carrier(std::shared_ptr<Carrier>) | type classes Wrap db classes and extract high level data on demand
//
// bad:
// - "slow" getters/setters always need to convert
// - when searching for a carrier based on high level stuff type::Carriers have to
//   be created and destroyed all the time
// - we always geht a std::vector<type::Carrier> from all carriers and need to allocate
//   and deallocate the whole stuff over and over

// new state:
// intentions:
// - not threadsafe
// - fast
// - expect many more reads than writes
// - expect many iterations through object lists
// - ensure obj gets updated after modification

// assumptions:
// - getting a pointer back is not always a bad thing?

#include <algorithm>
#include <list>
#include <vector>
#include <functional>
#include <ranges>

#include <string> // DBG
#include <iostream> // DBG

class NonCopyable
{
public:
    NonCopyable() = default;
    NonCopyable(NonCopyable const&) = delete;
    NonCopyable& operator=(NonCopyable const&) = delete;
};

// References increment reference counters and check if the object is valid
template <class T>
class Ref
{
public:
	using value_type = T;

    explicit Ref(T const& obj)
    : _obj(&const_cast<T&>(obj))
    {
		_obj->add_ref(*this);
	}

	Ref(Ref const& orig)
		: _obj(orig._obj)
	{
		if (_obj != nullptr)
			_obj->add_ref(*this);
	}

	Ref(Ref&& orig)
		: _obj(std::move(orig._obj))
	{
		if (_obj != nullptr)
		{
			_obj->add_ref(*this);
			_obj->del_ref(orig);
			orig._obj = nullptr;
		}
	}

	~Ref()
	{
		if (_obj != nullptr)
			_obj->del_ref(*this);
	}

    bool is_valid() const
    {
        return _obj != nullptr and not _obj->is_removed();
    }

	void reset()
	{
		if (_obj != nullptr)
		{
			_obj->del_ref(*this);
			_obj = nullptr;
		}
	}

    typename T::value_type const* get() const
    {
        return static_cast<typename T::value_type const*>(_obj);
    }

    typename T::value_type const* operator->() const
    {
		return get();
    }

	Ref& operator=(Ref const& orig)
	{
		if (_obj != nullptr)
			_obj->del_ref(*this);

		_obj = orig._obj;
		if (_obj != nullptr)
			_obj->add_ref(*this);
	}

	Ref& operator=(Ref&& orig)
	{
		if (_obj != nullptr)
			_obj->del_ref(*this);

		_obj = std::move(orig._obj);
		if (_obj != nullptr)
		{
			_obj->add_ref(*this);
			_obj->del_ref(orig);
			orig._obj = nullptr;
		}
	}

	
private:
	T* _obj = nullptr;
};

template <class T>
class Mutable
{
public:
	using value_type = T;

    explicit Mutable(Ref<T> obj)
    : _obj(std::move(obj))
    {}

    ~Mutable()
    {
		if (not _obj.is_valid())
			return;

		if (_obj->is_removed())
			return;

		if (not _obj->is_modified())
			return;

		const_cast<typename T::value_type*>(_obj.get())->update();
    }

    typename T::value_type* operator->() // this
    {
		const_cast<typename T::value_type*>(_obj.get())->modified();
        return const_cast<typename T::value_type*>(static_cast<typename T::value_type const*>(_obj.get()));
    }

    [[deprecated("use ->")]] typename T::value_type& modify() // or this?
    {
		const_cast<typename T::value_type*>(_obj.get())->modified();
        return const_cast<typename T::value_type&>(static_cast<typename T::value_type const&>(*(_obj.get())));
    }

private:
    Ref<T> _obj; // counted reference required since update is fired after removal
};

template <class T>
class EventHandler
{
public:
	virtual void inserted(T&) = 0;
	virtual void updated(T&) = 0;
	virtual void removed(T&) = 0;
};

template <class T>
class Obj : public NonCopyable
{
    friend T;
    friend Mutable<T>;
	friend Ref<T>;

public:
    using value_type = T;

	Obj(EventHandler<value_type>& handler)
		: _handler(handler)
	{}

	~Obj()
	{
		for (auto ref : _refs)
			ref->reset();
	}

    bool is_persistent() const { return _persistent; }
    bool is_removed() const { return _removed; } // I guess this is pointless

    Mutable<T> mutate() const
    {
		return Mutable<T>(keep());
    }

    Ref<T> keep() const
    {
		return Ref<T>(*const_cast<T*>(static_cast<T const*>(this)));
    }

    T& update()
    {
        _modified = false;
        if (_persistent) { emit_updated(); }
        else if (not _removed) {
            _persistent = true;
            emit_inserted();
        }

        return *static_cast<T*>(this);
    }
    T& remove() // should not reqturn a reference?
    {
        if (_persistent)
        {
            _persistent = false;
            _removed = true;
            emit_removed();
        }
        
        return *static_cast<T*>(this);
    }

private:
    void emit_updated()  { _handler.updated(static_cast<value_type&>(*this)); };
    void emit_inserted() { _handler.inserted(static_cast<value_type&>(*this)); };
    void emit_removed()  { _handler.removed(static_cast<value_type&>(*this)); };

    void modified()
    {
        _modified = true;
    }
    bool is_modified() const
    {
        return _modified;
    }

	void add_ref(Ref<T>& ref)
	{
		_refs.push_back(&ref);
	}

	void del_ref(Ref<T>& ref)
	{
		_refs.erase(std::find_if(_refs.begin(), _refs.end(), [&](const auto* r) {return &ref == r;}));
	}

private:
	EventHandler<value_type>& _handler;
	std::vector<Ref<T>*> _refs; // or list?
    bool _persistent = false;
    bool _removed    = false;
    bool _modified   = false;
};

template <class T>
class Objs : public EventHandler<T>
{
    friend T;
    friend typename T::Data;
    static_assert(std::is_base_of<Obj<T>, T>::value);

public:
    template <class... Args>
    T const& add(Args... args) // return & inconsistent to * from find? return nullptr if id exists? also no duplicate check atm
    {
        _objs.emplace_back(*this, args...);
        return _objs.back();
    }
    
    auto begin() { return _objs.begin(); }
    auto end() { return _objs.end(); }
    auto cbegin() const { return _objs.cbegin(); }
    auto cend() const { return _objs.cend(); }
    auto rbegin() { return _objs.rbegin(); }
    auto rend() { return _objs.rend(); }
    auto crbegin() const { return _objs.crbegin(); }
    auto crend() const { return _objs.crend(); }

private:
    void inserted(T& obj) override
    {
        obj.into(_prealloc_data);
        // TODO storage.insert(_pralloc_data)
        // TODO emit event
    }
    void updated(T& obj) override
    {
        obj.into(_prealloc_data);
        // TODO storage.update(_pralloc_data)
        // TODO emit event
    }
    void removed(T& obj) override
    {
        // TODO emit event
        
        obj.into(_prealloc_data);
		// TODO emit event
        // TODO storage.remove(_pralloc_data)
		_objs.erase(std::find_if(_objs.begin(), _objs.end(), [&](const auto& e) {return &obj == &e;}));

    }

private:
    std::list<T>     _objs; // list vs vector in order to not require ptr? later use pmr::list for better memory order
    typename T::Data _prealloc_data; // to safe allocations?
};
