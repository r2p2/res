#pragma once
#include "mem.hpp"

class Carrier : public Obj<Carrier>
{
    friend Objs<Carrier>;
public:
    enum class State
    {
        Unknown,
        WaitIn,
        WaitOut,
        Transferring,
        Stored,
    };

    Carrier() = delete;
    explicit Carrier(EventHandler<Carrier>& eh, std::string id)
		: Obj(eh)
        , _id(std::move(id))
    {
    }

    std::string id() const { return _id; }
    std::string loc_curr() const { return _loc_curr; };
    
    Carrier& loc_curr(std::string const& new_loc)
    {
        _loc_curr = new_loc;
        return *this;
    }

    Carrier& state(State new_state)
    {
        _state = new_state;
        return *this;
    }

    bool is_stored() const { return _state == State::Stored; }
    // ...

    std::string str() const
    {
        std::string s = "carrier{";
        s += is_persistent() ? 'P' : 'p';
        s += is_removed() ? 'R' : 'r';
        s += " id:'";
        s += _id;
        s += "', loc_curr:'";
        s += _loc_curr;
        s += "', state:'";
        switch(_state)
        {
        case State::Unknown: s += "unknown"; break;
        case State::WaitIn: s += "waitin"; break;
        case State::WaitOut: s += "waitout"; break;
        case State::Transferring: s += "transferring"; break;
        case State::Stored: s += "stored"; break;
        }
        s += "'}";
        return s;
    }

private:
    // BEGIN required from Obj
    struct Data // Like it is stored in the storage
    {
        std::string id;
        std::string loc_curr;
        std::string state;
    };
    explicit Carrier(EventHandler<Carrier>& eh, Data const& data)
		: Obj(eh)
    {
        from(data);
    }
    void into(Data& data)
    {
        data.id = _id;
        data.loc_curr = _loc_curr;

        switch(_state)
        {
        case State::Unknown: data.state = "unknown"; break;
        case State::WaitIn: data.state = "in"; break;
        case State::WaitOut: data.state = "out"; break;
        case State::Transferring: data.state = "transferring"; break;
        case State::Stored: data.state = "stored"; break;
        }
    }
    void from(Data const& data)
    {
        _id = data.id;
        _loc_curr = data.loc_curr;
        switch(data.state.front())
        {
            case 'i': _state = State::WaitIn; break;
            case 'o': _state = State::WaitOut; break;
            case 't': _state = State::Transferring; break;
            case 's': _state = State::Stored; break;
            default: _state = State::Unknown; break;
        }
    }
    // END required from Obj

private:
    std::string _id; // :( may not be const due to from(Data)
    std::string _loc_curr;
    State       _state;
};

class Carriers : public Objs<Carrier>
{
public:
    Carrier const* find_by_id(std::string const& id)
    {
        for (auto const& carrier : *this)
            if (carrier.id() == id)
                return &carrier;
            
        return nullptr;
    }

private:

};
